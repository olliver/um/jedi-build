
## How to build a new image release

First, you need to release the packages that you want to have new changes in in this build.

Releasing a package is easy. The command:
```
./build.sh release [package_name] [version] [branch]
```
will release this package. It will check out the fresh default branch, build the package, commit the package to the package repository and tag the sources with the version number.

For version numbering, we currently use the date (2016.01.18 for example) which can be postfixed with a number for multiple releases on the same day. 2016.01.18-2

To check if a package actually contains changes, you can checkout the latest sources, and use:
```
git diff `git tag | tail -n 1`
```
## Building the actual image release

You build the actual image with:
```
./build.sh release image [version]
```
Images use a different versioning scheme. The current date is automatically added. For pre-releases we use version 2.99.0-dev, if an update needs to be released on the same day, you can use 2.99.1-dev

To finally upload the image to the proper location for updating the machine, the upload.sh script is provided. Note that this is designed to run on the monolith machine, as it places the resulting image in a folder there as well. It also requires a proper .netrc for ftp username and password.
```
VERSION=[version_with_date] RELEASE_TYPE=[internal|testing|stable] ./upload.sh
```
## Tip for faster image build
Use multiple processors! (don't do this on our monolith server)
Prepend the make command with: MAKEFLAGS=j5   (choose a number 1 higher than the number of processors in your computer).

## Example, build using master repositories

Release done on 18-01-2016:
```
./build.sh release opinicus 2016.01.18
./build.sh release jedi-display 2016.01.18
PACKAGE_LIST_FILE=./um3-packages.list RELEASE_TYPE=testing ./build.sh release image 2.99.0
VERSION=2.99.0.20160118 RELEASE_TYPE=test ./upload.sh
```


## Example, build for S5, having repository package branch XL and only internal.

Release done on 2018-04-11:
```
PACKAGE_REPO_BRANCH=XL ./build.sh release um-kernel 2018.02.08 OK-130
PACKAGE_REPO_BRANCH=XL ./build.sh release opinicus 2018.04.03
PACKAGE_LIST_FILE=./s5-packages.list PACKAGE_REPO_BRANCH=XL ./build.sh release fdm_materials 2018.04.11 3.3
PACKAGE_LIST_FILE=./s5-packages.list PACKAGE_REPO_BRANCH=XL RELEASE_TYPE=internal ./build.sh release image 5.0.1

Firmware upload example for the S5 (9051) internal version
./upload.sh -v 5.0.1.20180411 -t internal -a 9051 [-i optional firmware files location] [-u optional ssh username]

For backwards compatibility use:
[SSH_USER=raymond] ARTICLE_NUMBERS=9051 VERSION=5.0.2.20180410 RELEASE_TYPE=internal ./upload.sh
```

## For Taranis NFC stations
```
PACKAGE_LIST_FILE=taranis-packages.list PACKAGE_REPO_BRANCH=taranis ./build.sh build taranis
PACKAGE_LIST_FILE=taranis-packages.list PACKAGE_REPO_BRANCH=taranis ./build.sh build image
PACKAGE_LIST_FILE=taranis-packages.list PACKAGE_REPO_BRANCH=taranis ./build.sh release image <version>
```
