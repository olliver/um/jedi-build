#!/bin/sh
#
#    Copyright (c) 2015 Ultimaker B.V.
#    Author: Olliver Schinagl <oliver@schinagl.nl>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

set -e

if [ -z ${TARGET+x} ]; then
	printf "\$TARGET needs to be set.\n"
	exit 1
fi

mkdir -p ${TARGET}./etc/modprobe.d/
MODULE=${TARGET}./etc/modprobe.d/ssd1307fb.conf
cat > ${MODULE} <<-EOT
options ssd1307fb refreshrate=15
EOT
mkdir -p ${TARGET}./etc/modules-load.d/
MODULE=${TARGET}./etc/modules-load.d/opinicus_modules.conf
cat > ${MODULE} <<-EOT
ssd1307fb
i2c-dev
EOT
