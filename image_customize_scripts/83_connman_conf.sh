#!/bin/sh

# Create a connman configuration with preferers ethernet, and only connects with a single technology at a time.
# This to prevent double IP addresses and the complexity of being connected to two different networks at the same time.
mkdir -p ${TARGET}/etc/connman/
cat > ${TARGET}/etc/connman/main.conf <<-EOT
[General]
PreferredTechnologies=ethernet,wifi
SingleConnectedTechnology=true
EOT
