#!/bin/sh

if [ ! -x "${TARGET}/var/lib/dpkg/info/usbmount.preinst" ]; then
	exit 0
fi

chroot ${TARGET} /var/lib/dpkg/info/usbmount.preinst install

# For Taranis, USB sticks (NTFS formatted) must be mounted in /media/ultimaker/nfc_config_pub
if [ -z "${PACKAGE_LIST_FILE##*taranis*}" ]; then
	mkdir -p ${TARGET}/media/ultimaker/nfc_config_pub
	cat > ${TARGET}/etc/usbmount/usbmount.conf <<-EOT
# Configuration file for the usbmount package, which mounts removable
# storage devices when they are plugged in and unmounts them when they
# are removed.

# Change to zero to disable usbmount
ENABLED=1

# Mountpoints: These directories are eligible as mointpoints for
# removable storage devices.  A newly plugged in device is mounted on
# the first directory in this list that exists and on which nothing is
# mounted yet.
MOUNTPOINTS="/media/ultimaker/nfc_config_pub /media/usb0 /media/usb1 /media/usb2 /media/usb3
             /media/usb4 /media/usb5 /media/usb6 /media/usb7"

# Filesystem types: removable storage devices are only mounted if they
# contain a filesystem type which is in this list.
FILESYSTEMS="vfat ntfs fuseblk ext2 ext3 ext4 hfsplus"

# Mount options: Options passed to the mount command with the -o flag.
MOUNTOPTIONS="rw,noexec,nodev,noatime,nodiratime,uid=ultimaker"

# Filesystem type specific mount options: This variable contains a space
# separated list of strings, each which the form "-fstype=TYPE,OPTIONS".
#
# If a filesystem with a type listed here is mounted, the corresponding
# options are appended to those specificed in the MOUNTOPTIONS variable.
#
# For example, "-fstype=vfat,gid=floppy,dmask=0007,fmask=0117" would add
# the options "gid=floppy,dmask=0007,fmask=0117" when a vfat filesystem
# is mounted.
FS_MOUNTOPTIONS="-fstype=ntfs-3g,nls=utf8,umask=007,gid=46
-fstype=fuseblk,nls=utf8,umask=007,gid=46 -fstype=vfat,gid=1000,uid=1000,umask=007"

# If set to "yes", more information will be logged via the syslog
# facility.
VERBOSE=no
EOT
else
	cat > ${TARGET}/etc/usbmount/usbmount.conf <<-EOT
# Configuration file for the usbmount package, which mounts removable
# storage devices when they are plugged in and unmounts them when they
# are removed.

# Change to zero to disable usbmount
ENABLED=1

# Mountpoints: These directories are eligible as mointpoints for
# removable storage devices.  A newly plugged in device is mounted on
# the first directory in this list that exists and on which nothing is
# mounted yet.
MOUNTPOINTS="/media/usb0 /media/usb1 /media/usb2 /media/usb3
             /media/usb4 /media/usb5 /media/usb6 /media/usb7"

# Filesystem types: removable storage devices are only mounted if they
# contain a filesystem type which is in this list.
FILESYSTEMS="vfat ext2 ext3 ext4 hfsplus"

# Mount options: Options passed to the mount command with the -o flag.
MOUNTOPTIONS="ro,noexec,nodev,noatime,nodiratime,uid=ultimaker"

# Filesystem type specific mount options: This variable contains a space
# separated list of strings, each which the form "-fstype=TYPE,OPTIONS".
#
# If a filesystem with a type listed here is mounted, the corresponding
# options are appended to those specificed in the MOUNTOPTIONS variable.
#
# For example, "-fstype=vfat,gid=floppy,dmask=0007,fmask=0117" would add
# the options "gid=floppy,dmask=0007,fmask=0117" when a vfat filesystem
# is mounted.
FS_MOUNTOPTIONS=""

# If set to "yes", more information will be logged via the syslog
# facility.
VERBOSE=no
EOT
fi
