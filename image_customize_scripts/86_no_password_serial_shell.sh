#!/bin/sh

# Change the systemd serial service so we auto login as root on the serial shell.
# You have physical access, so the keys to the kingdom are yours.

cat > ${TARGET}/etc/systemd/system/serial-getty@.service <<-EOT
# Copy of the system serial-getty@.service
[Unit]
Description=Serial Getty on %I
Documentation=man:agetty(8) man:systemd-getty-generator(8)
Documentation=http://0pointer.de/blog/projects/serial-console.html
BindsTo=dev-%i.device
After=dev-%i.device systemd-user-sessions.service plymouth-quit-wait.service
After=rc-local.service

# If additional gettys are spawned during boot then we should make
# sure that this is synchronized before getty.target, even though
# getty.target didn't actually pull it in.
Before=getty.target
IgnoreOnIsolate=yes

[Service]
ExecStart=-/sbin/agetty -a root --keep-baud 115200,38400,9600 %I $TERM
Type=idle
Restart=always
UtmpIdentifier=%I
TTYPath=/dev/%I
TTYReset=yes
TTYVHangup=yes
KillMode=process
IgnoreSIGPIPE=no
SendSIGHUP=yes

[Install]
WantedBy=getty.target
EOT
ln -sf /etc/systemd/system/getty.target.wants/serial-getty@ttyS0.service ${TARGET}/etc/systemd/system/serial-getty@.service

cat >> ${TARGET}/etc/motd <<-EOT
   __  ______  _                 __
  / / / / / /_(_)___ ___  ____ _/ /_____  _____
 / / / / / __/ / __ '__ \/ __ '/ //_/ _ \/ ___/
/ /_/ / / /_/ / / / / / / /_/ / ,< /  __/ /
\____/_/\__/_/_/ /_/ /_/\__,_/_/|_|\___/_/
Welcome to our kingdom. Please be responsible.
EOT
