#!/bin/sh
#
# Copyright (c) 2018 Ultimaker B.V.
# Copyright (c) 2018 Attila Body <a.body@ultimaker.com>
# Copyright (c) 2018 Olliver Schinagl <oliver@schinagl.nl>
#
# SPDX-License-Identifier: AGPL-3.0+

set -e

if [ -z ${TARGET+x} ]; then
	printf "Must supply target to script.\n"
	exit 1
fi

# TODO EMP-475 Work around gpg and gpgv file finding.
cat public_key.gpg | chroot "${TARGET}" sh -c "HOME=/root /usr/bin/gpg --import -"
chroot "${TARGET}" sh -c "ln -fs pubring.gpg /root/.gnupg/trustedkeys.gpg && \
                          ln -fs pubring.kbx /root/.gnupg/trustedkeys.kbx"
if [ -d "${TARGET}/home/ultimaker" ]; then
    chroot "${TARGET}" sh -c "cp -r /root/.gnupg /home/ultimaker/ && \
                              chown -R ultimaker:ultimaker /home/ultimaker/.gnupg "
fi

exit 0
