#!/bin/sh
#
#    Copyright (c) 2014 Ultimaker B.V.
#    Author: Olliver Schinagl <oliver@schinagl.nl>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

set -e

if [ -z ${TARGET+x} ]; then
	printf "\$TARGET needs to be set.\n"
	exit 1
fi

FSTAB=${TARGET}./etc/fstab
cat > ${FSTAB} <<-EOT
# /etc/fstab: static file system information.
#
# <file system> <mount point>   <type>  <options>       <dump>  <pass>
# This line is here so the root filesystem gets mounted as read/write, effectively "mount -a" is ran after boot, which sets the proper mount options.
# It ignores the <type> and <file system> if the mount point is already mounted.
rootfs          /               rootfs  defaults,noatime 0       1
# Add a temp filesystem on /tmp, this makes /tmp in memory.
tmpfs           /tmp            tmpfs   defaults         0       0
# Add tmpfs mounts for apt-get usage
tmpfs           /var/lib/apt    tmpfs   defaults         0       0
tmpfs           /var/cache/apt  tmpfs   defaults         0       0
EOT
