#!/bin/sh

cat > ${TARGET}/etc/rc.local <<-EOT
#!/bin/sh

#setup the hostname to include the MAC address of LAN port.
hostnamectl set-hostname UltimakerSystem-\$(cat /sys/class/net/eth0/address | sed 's/://g')
if [ "\`cat hosts | grep \$(cat /etc/hostname)\`" = "" ]; then
    # Make sure the hostname is in the hosts file, when it is not sudo is very slow.
    echo 127.0.0.1 \$(cat /etc/hostname) >> /etc/hosts;
fi

#Loading the LM75B driver from the kernel device tree is currently not possible.
#So load it after boot. This will fail harmlessly if the address is already in use or the LM75B cannot be found.
echo "lm75b 0x4f" > /sys/class/i2c-adapter/i2c-3/new_device

#set the capabilities of python to allow to listen on ports below 1024, and allow access to tty for the beeper functionality.
#as well as capability to change the system time. (Note, in debian stretch we can solve this from systemd)
setcap cap_net_admin,cap_dac_override,cap_sys_tty_config,cap_net_bind_service,cap_sys_time+ep /usr/bin/python3.4
# Avahi needs to run after rc.local is run. This is because avahi seems to cache the hostname, causing all kinds of problems with duplicate "Hostname not set" hostnames.
# TODO EM-366
sleep 2s
systemctl restart avahi-daemon.service

exit 0
EOT
chmod +x ${TARGET}/etc/rc.local

