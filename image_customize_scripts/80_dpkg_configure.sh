#!/bin/bash

mount --bind /dev ${TARGET}dev
mount --bind /proc ${TARGET}proc
mount --bind /sys ${TARGET}sys

trap "umount -lR ${TARGET}dev || true; umount -lR ${TARGET}proc || true; umount -lR ${TARGET}sys || true" exit

# Setup the policy-rc.d to preveny any service from starting.
cat > "${TARGET}/usr/sbin/policy-rc.d" <<EOF
#!/bin/sh
exit 101
EOF
chmod a+x "${TARGET}/usr/sbin/policy-rc.d"
# Prevent the start-stop-daemon from being used during package configuration, replace it with an empty script.
mv "${TARGET}/sbin/start-stop-daemon" "${TARGET}/sbin/start-stop-daemon.REAL"
cat > "${TARGET}/sbin/start-stop-daemon" <<EOF
#!/bin/sh
EOF
chmod a+x "${TARGET}/sbin/start-stop-daemon"


DEBIAN_FRONTEND=noninteractive DEBCONF_NONINTERACTIVE_SEEN=true \
	LC_ALL=C LANGUAGE=C LANG=C chroot ${TARGET} /var/lib/dpkg/info/dash.preinst install
chmod -x ${TARGET}/etc/init.d/dropbear
DEBIAN_FRONTEND=noninteractive DEBCONF_NONINTERACTIVE_SEEN=true \
	LC_ALL=C LANGUAGE=C LANG=C chroot ${TARGET} \
	dpkg --configure -a
chmod +x ${TARGET}/etc/init.d/dropbear


# Revert the changes we did to prevent service starting.
rm "${TARGET}/usr/sbin/policy-rc.d"
mv "${TARGET}/sbin/start-stop-daemon.REAL" "${TARGET}/sbin/start-stop-daemon"

umount ${TARGET}dev || true
umount ${TARGET}proc || true
umount ${TARGET}sys || true
