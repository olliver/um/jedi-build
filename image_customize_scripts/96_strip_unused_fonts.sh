#!/bin/bash

# Delete all font files we don't really need.
# All we do need is NotoSans(CJK) in Regular and Bold weights

set -e

if [ -z "${TARGET}" ]; then
    exit 1
fi

if [ ! -d "${TARGET}/usr/share/fonts" ]; then
    exit 0
fi


okuda_fonts=("NotoSans-Bold.ttf" "NotoSans-Regular.ttf" "NotoSansCJK-Bold.ttc" "NotoSansCJK-Regular.ttc")
jedi_fonts=("*.pcf.gz")
taranis_fonts=("*DejaVu*")  # This is some 2.8 MB but keeps the default fonts, in case all else fails
license=("*license*" "*copyright*")

keep=("${okuda_fonts[@]}" "${jedi_fonts[@]}"  "${taranis_fonts[@]}" "${license[@]}")

filter=""

echo "Keeping only font files ${keep[*]}"
echo "Deleting ALL other font files"

for item in ${keep[*]};
do
    filter+=" -not -name ${item} "
done

find ${TARGET}/usr/share/fonts/*/* -type f $filter -exec rm {} +
