#!/bin/sh
#
#    Copyright (c) 2015 Ultimaker B.V.
#    Author: Olliver Schinagl <oliver@schinagl.nl>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

set -e

if [ -z ${TARGET+x} ]; then
	printf "\$TARGET needs to be set.\n"
	exit 1
fi

UDEVRULES=${TARGET}./etc/udev/rules.d/
cat > ${UDEVRULES}/10-hide_partitions.rules <<-EOT
# Hide built-in partitions
KERNEL=="mmcblk0p1", ENV{UDISKS_PRESENTATION_HIDE}="1"
KERNEL=="mmcblk0p2", ENV{UDISKS_PRESENTATION_HIDE}="1"
KERNEL=="mtd0", ENV{UDISKS_PRESENTATION_HIDE}="1"
KERNEL=="mtd1", ENV{UDISKS_PRESENTATION_HIDE}="1"
KERNEL=="mtd2", ENV{UDISKS_PRESENTATION_HIDE}="1"
EOT

cat > ${UDEVRULES}/99-udisks2.rules <<-EOT
# UDISKS_FILESYSTEM_SHARED
# ==1: mount filesystem to a shared directory (/media/VolumeName)
# ==0: mount filesystem to a private directory (/run/media/$USER/VolumeName)
# See udisks(8)
ENV{ID_FS_USAGE}=="filesystem|other|crypto", ENV{UDISKS_FILESYSTEM_SHARED}="1"
EOT

cat > ${UDEVRULES}/00-leds.rules <<-EOT
KERNEL=="pca963x:*", RUN+="/bin/sh -c 'chgrp -R leds /sys\$DEVPATH; chmod g+w -R /sys\$DEVPATH'"
EOT

cat > ${UDEVRULES}/00-gpio.rules <<-EOT
KERNEL=="gpio*", RUN+="/bin/sh -c 'chgrp -R gpio /sys\$DEVPATH; chmod g+w -R /sys\$DEVPATH'"
EOT

# Make sure that i2c devices can be accessed by the ultimaker user.
cat > ${UDEVRULES}/00-i2c.rules <<-EOT
KERNEL=="i2c-*", GROUP="ultimaker" OWNER="ultimaker"
EOT

cat > ${UDEVRULES}/99-eeprom.rules <<-EOT
KERNEL=="1-0050", ATTR{name}=="24c16", RUN+="/bin/sh -c 'chgrp -R eeprom /sys\$DEVPATH; chmod g+r -R /sys\$DEVPATH'"
EOT

cat > ${UDEVRULES}/99-display-backlight.rules <<-EOT
SUBSYSTEM=="backlight", RUN+="/bin/sh -c 'chgrp -R video /sys\$DEVPATH; chmod g+rw -R /sys\$DEVPATH'"
EOT

