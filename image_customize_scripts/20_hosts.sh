#!/bin/sh
#
#    Copyright (c) 2014 Ultimaker B.V.
#    Author: Olliver Schinagl <oliver@schinagl.nl>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

set -e

if [ -z ${TARGET+x} ]; then
	printf "Must supply target to script.\n"
	exit 1
fi

HOSTS=${TARGET}./etc/hosts
printf "# /etc/hosts: Local Host Database\n" > ${HOSTS}
printf "#\n" >> ${HOSTS}
printf "# This file describes a number of aliases-to-address mappings for the for\n" >> ${HOSTS}
printf "# local hosts that share this file.\n" >> ${HOSTS}
printf "#\n" >> ${HOSTS}
printf "# In the presence of the domain name service or NIS, this file may not be\n" >> ${HOSTS}
printf "# consulted at all; see /etc/host.conf for the resolution order.\n" >> ${HOSTS}
printf "#\n" >> ${HOSTS}
printf "\n" >> ${HOSTS}
printf "# IPv4 and IPv6 localhost aliases\n" >> ${HOSTS}
printf "127.0.0.1       localhost.localdomain localhost\n" >> ${HOSTS}
printf "::1             localhost.localdomain localhost\n" >> ${HOSTS}
printf "\n" >> ${TARGET}./etc/hosts
