#!/bin/sh
#
#    Copyright (c) 2014 Ultimaker B.V.
#    Author: Roy Spliet <r.spliet@ultimaker.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

set -e

if [ -z ${TARGET+x} ]; then
	printf "Must supply target to script.\n"
	exit 1
fi

# Create the user ultimaker in case it does not exist yet.
chroot "${TARGET}" useradd -m -s /bin/bash ultimaker || true

groupadd -R "${TARGET}" -r opinicus
groupadd -R "${TARGET}" -r gpio
groupadd -R "${TARGET}" -r leds
groupadd -R "${TARGET}" -r eeprom
chroot "${TARGET}" usermod -a -G opinicus ultimaker
# Add ultimaker to the dialout group so we can access the serial ports.
chroot "${TARGET}" usermod -a -G dialout ultimaker
# Add ultimaker to the video group so we can access the framebuffer.
chroot "${TARGET}" usermod -a -G video ultimaker
# Add ultimaker to the input group so we can access the input devices.
chroot "${TARGET}" usermod -a -G input ultimaker
# Add ultimaker to the gpio group so we can access the gpios.
chroot "${TARGET}" usermod -a -G gpio ultimaker
# Add ultimaker to the leds group so we can access the leds.
chroot "${TARGET}" usermod -a -G leds ultimaker
# Add ultimaker to the eeprom group so we can read the eeprom.
chroot "${TARGET}" usermod -a -G eeprom ultimaker
# Add ultimaker to the tty group so we can access the beeper.
chroot "${TARGET}" usermod -a -G tty ultimaker

# Change the shell of the ultimaker user to the commandline tool
chroot "${TARGET}" usermod -s /usr/share/griffin/command_util.py ultimaker
echo "/usr/share/griffin/command_util.py" >> ${TARGET}/etc/shells


echo "ultimaker:ultimaker" | chpasswd -c SHA512 -R "${TARGET}"
echo "root:ultimaker" | chpasswd -c SHA512 -R "${TARGET}"
# Hack to add root to the group ultimaker for now. This will be removed when UI no longer requires root.
chroot "${TARGET}" usermod -a -G opinicus,ultimaker root
