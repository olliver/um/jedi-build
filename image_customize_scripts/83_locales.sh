#!/bin/sh

if [ -z "${TARGET}" ]; then
    exit 0
fi

if [ ! -x "${TARGET}/usr/sbin/locale-gen" ]; then
    # locale-gen is not available so we assume locales package is not installed
    # which means we should not do anything.
    exit 0
fi

cat > "${TARGET}/etc/locale.gen" <<-EOT
# This file lists locales that you wish to have built. You can find a list
# of valid supported locales at /usr/share/i18n/SUPPORTED, and you can add
# user defined locales to /usr/local/share/i18n/SUPPORTED. If you change
# this file, you need to rerun locale-gen.

en_US.UTF-8 UTF-8
EOT

chroot "${TARGET}" locale-gen

echo "LANG=en_US.utf8" > "${TARGET}/etc/default/locale"
