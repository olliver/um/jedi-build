#!/bin/sh


set -e

if [ -z ${TARGET+x} ]; then
        printf "Must supply target to script.\n"
        exit 1
fi

for LOCALE in `ls ${TARGET}/usr/share/locale | grep -v 'en'`; do
	rm -rf ${TARGET}/usr/share/locale/${LOCALE}
done

rm -rf ${TARGET}/usr/share/man
rm -rf ${TARGET}/usr/share/doc
rm -rf ${TARGET}/var/cache/apt
rm -rf ${TARGET}/var/lib/apt
rm -rf ${TARGET}/usr/share/locale
rm -rf ${TARGET}/run
mkdir ${TARGET}/run
mkdir -p ${TARGET}/var/lib/apt
mkdir -p ${TARGET}/var/cache/apt
