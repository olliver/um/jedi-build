#!/bin/sh
#
#    Copyright (c) 2014 Ultimaker B.V.
#    Author: Olliver Schinagl <oliver@schinagl.nl>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

set -e

if [ -z ${TARGET+x} ]; then
	printf "\$TARGET needs to be set.\n"
	exit 1
fi

INTERFACES=${TARGET}./etc/network/interfaces
printf "# This file describes the network interfaces available on your system\n" > ${INTERFACES}
printf "# and how to activate them. For more information, see interfaces(5).\n" >> ${INTERFACES}
printf "\n" >> ${INTERFACES}
printf "# The loopback network interface\n" >> ${INTERFACES}
printf "auto lo\n" >> ${INTERFACES}
printf "iface lo inet loopback\n" >> ${INTERFACES}
