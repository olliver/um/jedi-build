#!/bin/sh

chroot ${TARGET} /usr/bin/qemu-arm-static /bin/busybox --install -s

# Busybox provides a readlink utility, but the system already has one.
# The one from busybox causes problems with apt-get, so remove it.
rm -rf ${TARGET}/usr/bin/readlink
