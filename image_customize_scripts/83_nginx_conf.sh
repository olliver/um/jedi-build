#!/bin/sh

nginx_path="${TARGET}/etc/nginx/"
nginx_conf="${nginx_path}/nginx.conf"

if [ ! -f "${nginx_conf}" ]; then
	exit 0
fi

# Setup the nginx configuration.
# Debian creates a default nginx configuration which is a bit overkill.

# Set the amount of worker processes to 1, as the default is 4
sed -i 's/worker_processes.*/worker_processes 1;/g' ${nginx_conf}

# Redirect the log files to /dev/null.
# Nginx does not handle log rotation, and grows endlessly, we do not want to redirect to journalctl, because that will spam those logs as well.
sed -i 's/access_log.*/access_log \/dev\/null;/g' ${nginx_conf}
# We redirect error log for the same reason. Script errors are nog logged here,
# so we do not really need this log. (Script errors are in the fcgi process and end up in journalctl)
sed -i 's/error_log.*/error_log \/dev\/null;/g' ${nginx_conf}

# Override the default site configuration with our own configuration file.
cat > "${nginx_path}/sites-available/default" <<-EOT
# You should look at the following URL's in order to grasp a solid understanding
# of Nginx configuration files in order to fully unleash the power of Nginx.
# http://wiki.nginx.org/Pitfalls
# http://wiki.nginx.org/QuickStart
# http://wiki.nginx.org/Configuration

# Store temp files on /tmp in ram iso on the root partition hugely limiting the max filesize
client_body_temp_path /tmp;

server {
    listen 80 default_server;
    listen [::]:80 default_server;

    root /var/www/html;

    index index.html index.htm;

    server_name _;

    # Allow per package location configuration from /etc/nginx/locations
    include /etc/nginx/locations/*.conf;
}
EOT
