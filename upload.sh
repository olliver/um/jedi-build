#!/bin/bash
# This script is used to upload the release artifacts to the proper file locations for manual download and for
# automatic update functionality in the printers.
#
# Three firmware versions are possible; internal, testing and stable. As the naming suggests internal is only for
# internal usage and the latter two are made publicly available. All releases are uploaded or copied to one of the
# general locations, i.e.:
# The internal version's can be found on: http://monolith/releases/firmware/internal/
# The public version's can be found on: https://software.ultimaker.com/releases/firmware/stable/ and
# https://software.ultimaker.com/releases/firmware/testing/
#
# Whatever release type is given, symbolic links to the new version will be created in the "given" printer article
# directories, e.g. https://software.ultimaker.com/releases/firmware/9051/stable/[version]
#
# In order for the printer auto update functionality to work, links to the firmware files are created with special
# naming that the printer looks for, i.e. https://software.ultimaker.com/releases/firmware/9051/stable/firmware.tar.xz
# https://software.ultimaker.com/releases/firmware/9051/stable/firmware.sig, accompanied with a version file
# https://software.ultimaker.com/releases/firmware/9051/stable/version.txt
#
# Note: If the release type is 'testing' or 'internal' it is required to have a .netrc setup with the FTP credentials.

set -eu

RSYNC_OPTIONS="--progress --no-perms --no-owner --no-group --no-times"
SSH_OPTIONS="ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null"
CURL_OPTIONS="--ftp-ssl --ftp-create-dirs --disable-epsv -v -n"
INPUT_DIRECTORY="images"
HOSTNAME_INTERNAL_FIRMWARE="monolith"
WWWROOT_INTERNAL_FIRMWARE="/var/www/html"
HOSTNAME_EXTERNAL_FIRMWARE="software.ultimaker.com"
WWWROOT_EXTERNAL_FIRMWARE="/webroot/software"
GENERAL_FIRMWARE_PATH="releases/firmware"
EXCLUDE_FILE_PATTERN_EXTERNAL_FIRMWARE="*installer*"
TARGET_PATH_EXTERNAL_FIRMWARE="${WWWROOT_EXTERNAL_FIRMWARE}/${GENERAL_FIRMWARE_PATH}"
TARGET_PATH_INTERNAL_FIRMWARE="${WWWROOT_INTERNAL_FIRMWARE}/${GENERAL_FIRMWARE_PATH}"
DEPLOY_DIR="deploy"

function usage()
{
cat << EOT
Usage:      ./upload.sh -v software_version -t release_type -a article_number(s) [-i input_dir] [-u ssh username]
Example:    ./upload.sh -v 5.0.2.20180410 -t internal -a 9051,9066,9511 [-i /home/raymond/releases/502] -u raymond

            -a  A list of article numbers to release this version for
                1797 - PrintCore Programming Station
                9051 - S5
                9066 - UM3
                9404 - Watto Extended
                9502 - Ultimaker 2+
                9507 - Watto Go
                9511 - UM3Extended
            -h  Print usage help text
            -i  Optional input directory to look for firmware files, default is ./images
            -t  The release type, i.e. internal, testing or stable
            -u  The ssh username to use
            -v  The software version to upload, the build.sh script should output the artifacts with version number post-fixed

For backwards compatibility use:

[SSH_USER=raymond] ARTICLE_NUMBERS=9051,9066,9511 VERSION=5.0.2.20180410 RELEASE_TYPE=internal ./upload.sh

EOT
}

# Function that works like echo, but prints in red.
red() {
    echo -e -n "\033[0;31m"
    echo -n "$*"
    echo -e "\e[0m"
}

# Function that works like echo, but prints in red. Used to make the user notice an error.
# This function will always exit!
error() {
    red "$*" >&2
    exit 1
}

# Function that works like echo, but prints in red. Used to make the user notice an error.
# Prints the usage text as well.
# This function will always exit!
error_usage() {
    red "$*" >&2
	usage
    exit 1
}


# Sync the created deploy directory to the target over ssh
# and copy the firmware artifacts to that location
# $1 The target hostname
# $2 The target directory on the host
function sync_to_ssh_target()
{
    echo "Start SSH sync."

    if [[ -z ${1+x} ]]; then
        error "Hostname not provided, cannot continue sync over SSH."
    elif [[ -z ${2+x} ]]; then
        error "Target directory not provided, cannot continue sync over SSH."
    fi

    local hostname="${1}"
    local target_directory="${2}"
    local exclude_directive=""
    local target_url="${hostname}:${target_directory}"
    local user_target_url="${target_url}"

    if [[ "${hostname}" != "${HOSTNAME_INTERNAL_FIRMWARE}" ]]; then
        exclude_directive="--exclude ${EXCLUDE_FILE_PATTERN_EXTERNAL_FIRMWARE}"
    fi

    # If SSH_USER var is set use that, otherwise use the local user, if that fails ask for one
    if [[ -n ${SSH_USER+x} ]]; then
        user_target_url="${SSH_USER}@${target_url}"
    fi

    echo "Copying the smaller update files to: ${user_target_url}"
    # Upload the printer update files first with --max-size=150m
    if ! rsync -avzc ${RSYNC_OPTIONS} -e "${exclude_directive} ${SSH_OPTIONS}" --max-size=150m deploy/ "${user_target_url}"; then
        echo; echo "!! Unable to copy using default username or username set in SSH_USER, please provide a user name !!: "
        read SSH_USER
        echo
        user_target_url="${SSH_USER}@${target_url}"
        rsync -avzc  ${RSYNC_OPTIONS} -e "${exclude_directive} ${SSH_OPTIONS}" --max-size=150m deploy/ "${user_target_url}"
    fi

    echo "Copying the large image files to: ${user_target_url}"
    rsync -avzc  ${RSYNC_OPTIONS} -e "${exclude_directive} ${SSH_OPTIONS}" deploy/ "${user_target_url}"

    echo "End SSH sync"
}

# Push the firmware artifacts to the given path on the FTP server
# Symlinks are not supported at the moment, therefore create complete copies in the article
# directories for now.
# $1 The target hostname
# $2 The target directory on the host
# $3 The release type
# $4 The version
function copy_to_ftp_target()
{
    echo "Start copy to FTP server"

    if [[ -z ${1+x} ]]; then
        error "Hostname not provided, cannot continue copy over FTP."
    elif [[ -z ${2+x} ]]; then
        error "Target directory not provided, cannot continue copy over FTP."
    elif [[ -z ${3+x} ]]; then
        error "Release type not provided, cannot continue copy over FTP."
    elif [[ -z ${4+x} ]]; then
        error "Version not provided, cannot continue copy over FTP."
    fi

    local target_url="ftp://${1}${2}"
    local release_type=$3
    local version=$4

    pushd "${DEPLOY_DIR}"
    echo "Copying image artifacts to: ${target_url}"
    # Make copies of the files for the given article numbers
    for article_number in ${ARTICLE_NUMBERS[@]};do
        url="${target_url}/${article_number}/${release_type}"

        # First copy the complete version directory
        pushd "${release_type}"
        find . ! -name "${EXCLUDE_FILE_PATTERN_EXTERNAL_FIRMWARE}" -type f -exec curl ${CURL_OPTIONS} -T {} "${url}/"{} \;
        popd

        # Then add more copies
        pushd "${release_type}/${VERSION}"
        curl ${CURL_OPTIONS} -T "rootfs-${VERSION}.tar.xz" "${url}/firmware.tar.xz"
        curl ${CURL_OPTIONS} -T "rootfs-${VERSION}.tar.xz.sig" "${url}/firmware.sig"
        curl ${CURL_OPTIONS} -T "version.txt" "${url}/version.txt"

        # For the time being.. add even more copies too, when transition is stabilized this can be removed
        curl ${CURL_OPTIONS} -T "rootfs-${VERSION}.tar.xz" "${url}/latest.tar.xz"
        curl ${CURL_OPTIONS} -T "rootfs-${VERSION}.tar.xz.sig" "${url}/latest.tar.xz.sig"
        curl ${CURL_OPTIONS} -T "version.txt" "${url}/latest.version"
        popd
    done

    find "${release_type}/" ! -name "${EXCLUDE_FILE_PATTERN_EXTERNAL_FIRMWARE}" -type f -exec curl ${CURL_OPTIONS} -T {} "${target_url}/"{} \;
    popd

    echo "End copy to FTP server"
}

# Populate the deploy directory with firmware artifacts:
# deploy/
# deploy/[release type]
# deploy/[release type]/[version]
# deploy/[release type]/[version]/[firmware artifacts]
# deploy/[article number]/[release type]/[version] - Symbolic link
# deploy/[article number]/[release type]/[version]/firmware.tar.xz - Symbolic link
# deploy/[article number]/[release type]/[version]/firmware.sig - Symbolic link
# deploy/[article number]/[release type]/[version]/version.txt
# $1 Release type
function populate_deploy_directory()
{
    echo "Start populating deploy directory"

    if [[ -z ${1+x} ]]; then
        error "Release type not given, cannot create deploy directory"
    fi

    # Create and populate the deploy directory
    if [[ -d ./${DEPLOY_DIR} ]]; then
        rm -rf "./${DEPLOY_DIR}"
    fi

    local release_type=$1
    local target_directory="${DEPLOY_DIR}/${release_type}/${VERSION}/"

    mkdir -p "${target_directory}"
    cp "${INPUT_DIRECTORY}/rootfs-${VERSION}.tar.xz" "${target_directory}"
    cp "${INPUT_DIRECTORY}/rootfs-${VERSION}.tar.xz.sig" "${target_directory}"
    cp "${INPUT_DIRECTORY}/run_from_sd-${VERSION}.img" "${target_directory}"
    cp "${INPUT_DIRECTORY}/recovery-${VERSION}.img" "${target_directory}"
    cp version.txt "${target_directory}"
    cp "${INPUT_DIRECTORY}/UM3_installer-${VERSION}.img" "${target_directory}"
    cp "${INPUT_DIRECTORY}/UM3extended_installer-${VERSION}.img" "${target_directory}"
    cp "${INPUT_DIRECTORY}/S5_installer-${VERSION}.img" "${target_directory}"


    pushd "${DEPLOY_DIR}"

    # Create checklist file
    pushd "${release_type}/${VERSION}"
    if [[ -f "checklist.md5" ]]; then
        rm "checklist.md5"
    fi
    md5sum * > "checklist.md5"
    popd

    # Create article number directories and link firmware version and files
    for article_number in ${ARTICLE_NUMBERS[@]};do
        mkdir -p "${article_number}/${release_type}"
        pushd "${article_number}/${release_type}"
        ln -s "../../${release_type}/${VERSION}/" "${VERSION}"
        ln -s "../../${release_type}/${VERSION}/rootfs-${VERSION}.tar.xz" "firmware.tar.xz"
        ln -s "../../${release_type}/${VERSION}/rootfs-${VERSION}.tar.xz.sig" "firmware.sig"
        ln -s "../../${release_type}/${VERSION}/version.txt" "version.txt"

        # For the time being.. add the latest symlinks too, when transition is stabilized this can be removed
        ln -s "../../${release_type}/${VERSION}/rootfs-${VERSION}.tar.xz" "latest.tar.xz"
        ln -s "../../${release_type}/${VERSION}/rootfs-${VERSION}.tar.xz.sig" "latest.tar.xz.sig"
        ln -s "../../${release_type}/${VERSION}/version.txt" "latest.version"
        popd
    done

    popd

    echo "End populating deploy directory"
}

while getopts ":a:hi:t:u:v:" option; do
  case $option in
    a)
      ARTICLE_NUMBERS=${OPTARG}
      ;;
    h)
      usage
      exit 0
      ;;
    i)
      INPUT_DIRECTORY=${OPTARG}
      ;;
    t)
      RELEASE_TYPE=${OPTARG}
      ;;
    u)
      SSH_USER=${OPTARG}
      ;;
    v)
      VERSION=${OPTARG}
      ;;
    :)
      error_usage "Option -${OPTARG} requires an argument."
      ;;
    \?)
      error_usage "Invalid option: -${OPTARG}"
      ;;
  esac
done
shift "$((OPTIND-1))"

echo

if [[ -n ${ARTICLE_NUMBERS+x} ]]; then
    ARTICLE_NUMBERS=(${ARTICLE_NUMBERS//,/ })
fi

# Check pre-conditions
if [[ -z ${VERSION+x} ]]; then
	error_usage "Error: Version not given."
fi
if [[ -z ${RELEASE_TYPE+x} ]]; then
    error_usage "Error: Release type not given."
fi
if [[ -z ${ARTICLE_NUMBERS+x} || ${#ARTICLE_NUMBERS[@]} -eq 0 ]]; then
    error_usage "Error: No article numbers provided."
fi
if [[ "${RELEASE_TYPE}" != "internal" ]] && ! ( grep "machine ${HOSTNAME_EXTERNAL_FIRMWARE}" ~/.netrc ); then
    error "Error: FTP credentials for ${HOSTNAME_EXTERNAL_FIRMWARE} not found in ~/.netrc."
fi

echo "Input directory : ${INPUT_DIRECTORY}"
echo "Version : ${VERSION}"
echo "Article numbers: ${ARTICLE_NUMBERS}"
echo "Release type : ${RELEASE_TYPE}"

# Generate and upload the artifacts
echo ${VERSION} > version.txt
populate_deploy_directory "${RELEASE_TYPE}"

sync_to_ssh_target ${HOSTNAME_INTERNAL_FIRMWARE} "${TARGET_PATH_INTERNAL_FIRMWARE}"
if [[ "${RELEASE_TYPE}" != "internal" ]]; then
    copy_to_ftp_target ${HOSTNAME_EXTERNAL_FIRMWARE} "${TARGET_PATH_EXTERNAL_FIRMWARE}"\
        "${RELEASE_TYPE}" "${VERSION}"
fi
