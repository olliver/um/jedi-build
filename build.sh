#!/bin/bash

# Abort on errors, as well as unset variables. Makes the script less error prone.
set -o errexit
set -o nounset

# Find the location of this script, as some required things are stored next to it.
MYDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
# Set the directory where extra tools are located. (exported because the image customization scripts use this)
export TOOLS=${MYDIR}/tools/
# Toolchain file location for cmake
TOOLCHAIN_FILE=${TOOLS}/arm-cross-compile.toolchain
# Location where we store our source trees. Just one level above this, so we do not have git-in-git
PACKAGE_SOURCES=`realpath "${MYDIR}/../"`
# File that contains the list of packages
PACKAGE_LIST_FILE=${PACKAGE_LIST_FILE:-${MYDIR}/um3-packages.list}
# Location of the sysroot, which is used during cross compiling
SYSROOT=${TOOLS}/_sysroot
# Location of temporary build directory where intermittent files are present.
BUILD_DIR=${MYDIR}/_image_build
# Location of the released package repository, cloned from git.
PACKAGE_REPO=${MYDIR}/package-repository
PACKAGE_REPO_GIT=git@github.com:Ultimaker/jedi-package-repository.git
PACKAGE_REPO_BRANCH=${PACKAGE_REPO_BRANCH:-master}
# Location where package overrides are located for local building of images.
OVERRIDE_DIR=${MYDIR}/override/
# Filename of the resulting tar file when building an image
IMAGE_TAR_FILENAME=rootfs
UPDATE_FILENAME_PREFIX="um-update"
# Location if the SD card images
IMAGES_DIR=${MYDIR}/images
# Define release type
RELEASE_TYPE=${RELEASE_TYPE:=stable}

TARGET_ARCH=arm
CURRENT_ARCH=`dpkg --print-architecture`
# We use the 'type -P' command instead of 'which' since the first one is built-in, faster and has better defined exit values.
QEMU_BINARY=$(type -P qemu-${TARGET_ARCH}-static 2>/dev/null) || { echo "QEMU_${TARGET_ARCH}_NOT_FOUND"; exit 1; }
GPG=$(type -P gpg2 2>/dev/null || type -P gpg 2>/dev/null) || { echo "GPG binary cannot be found"; exit 1; }
SIGNING_KEY_ID="60DB37D0DCB365513901D8520B8F2C85A475C3E6"
PRIVATE_KEY_FILE="private_key.gpg"
PUBLIC_KEY_FILE="public_key.gpg"
GPG_PASSPHRASE_WARN="If needed, the passphrase for the key is: 'ultimaker'"

# All tags that we place on the repos will be prefixed with this prefix. Just some future proofing
TAG_PREFIX=jedi-${PACKAGE_REPO_BRANCH}-

MBR_BOOTLOADER_START=2048 # 1 MiB offset is a common and sane default.
MBR_BOOTLOADER_SIZE=65536 # 32 MiB
MBR_ROOTFS_START=$(expr ${MBR_BOOTLOADER_START} + ${MBR_BOOTLOADER_SIZE})

UBOOTENV=um_u-boot.env.bin

display_usage() {
    info "Usage:"
    info "${0} fetch [package]"
    info "  Will retrieve the source from the proper repository. Will remove any old checkout that might be there."
    info "${0} build [package]"
    info "  Builds a package from source. Will retrieve the source from the proper repository if no source has been checked out yet."
    info "[RELEASE_TYPE=stable|testing|internal] ${0} build image [sd|emmc]"
    info "  Builds a full system image. Will ask for sudo rights."
    info "  The sd parameter (default if ommited) builds an run_from_sd.img, the eMMC parameter builds an installer image to put on an sd card."
    info "  If the RELEASE_TYPE is ommitted, \"stable\" will be used as default. With stable release, some files wil be removed from the image "
    info "  before finishing: opinicus/okuda mocks, private material package, machine configurations that need to be kept a secret and so on."
    info "  For \"testing\" release, some packages might be removed as well, like private materials an machine configurations to be kept."
    info "  For details, see image_cleanup/ which holds the release files which describe what needs to be removed."
    info "${0} release [package] [version] ([hash/branch/tag])"
    info "  Builds and releases a package. Released package will be pushed to the package repository for inclusion in the next image build."
    info "  Note: This will cleanup the source tree of the package. Local changes will be purged."
    info "${0} deploy [package] [target ip]"
    info "  Builds the target package, and deploys it on the target machine."
    info "  Special case is when package is 'image' which will deploy the last built image."
    info "${0} sysroot"
    info "  Prepare a sysroot for cross compiling. Note that this will also be done automatically if there is no sysroot when trying to build a package."
    info "${0} listissues [package]"
    info "  List all the issues that are solved in this package since last release."
    info "${0} list-updated-packages"
    info "  List all the packages which have updates in the code that are not yet released."
    info "Required packages on Debian and Ubuntu:"
    info "sudo apt-get install realpath sshpass pkg-config packaging-dev multistrap git busybox binfmt-support qemu-user-static nodejs lzop device-tree-compiler u-boot-tools g++-4.9-arm-linux-gnueabihf gcc-4.9-arm-linux-gnueabihf arduino squashfs-tools xxd"
    info "Additional for Ubuntu 17+: sudo apt-get install gnupg1"
    info "sudo gem install bundler"
    info "cd griffin_html; bundler install; bundle exec jekyll build"
    info "Check the README.md in griffin_html for requirements on building that package."
    exit 1
}

###################
# Helper functions
###################

# Function that works like echo, but prints bold. Used to distinct between our output and output from tools we call.
info() {
    echo -e -n "\e[1m"
    echo -n "$*"
    echo -e "\e[0m"
}

# Function that works like echo, but prints in orange. Used to make the user notice a warning.
warn() {
    echo -e -n "\033[0;33m"
    echo -n "$*"
    echo -e "\e[0m"
}

# Function that works like echo, but prints in red. Used to make the user notice an error.
# This function will always exit!
error() {
    echo -e -n "\033[0;31m"
    echo -n "$*"
    echo -e "\e[0m"
    exit 1
}

# Function that runs the given command as soon as the script ends.
#  Uses traps, helper function is needed as we can only set a single trap.
#  at_exits are run in the reverse order as they are created. This to reverse what we created (for example, unmount before removing a loopback)
at_exit() {
    OLD_TRAP=`trap -p EXIT | awk -F"'" '{print $2}'`
    trap "set +e;$*;${OLD_TRAP}" EXIT
}

# Detects GPGv2 exact version and builds signing command accordingly
build_gpgsig_params()
{
    version=$( "${GPG}" --version | head -n 1 | cut -d ' ' -f 3 )
    major=$(echo "${version}" | cut -d '.' -f 1 )
    minor=$(echo "${version}" | cut -d '.' -f 2 )
    basecmd="--batch --passphrase-fd 0 --yes"

    if [ "${major}" -lt 2 ]; then
        error "GPGv2 is required, cannot continue.";
    fi

    basecmd="--batch --passphrase-fd 0 --yes"
    if [ "${minor}" -lt 2 ]; then
        GPGSIGPARAMS="${basecmd}"
    else
        GPGSIGPARAMS="${basecmd} --pinentry-mode loopback";
    fi
}

# Append a pgp signature to the end of a new file, followed by a 4 byte,
# size indicator of the signature file.
# Takes two requiremed arguments
# ${1}: the location to the input file
# ${2}: the output filename
append_signature()
{
    INPUT_FILE="${1}"
    OUTPUT_FILENAME="${2}"

    if [ -z "${GPGSIGPARAMS:-}" ]; then
        echo "Missing GPGSIGPARAMS, this should not happen."
        exit 1
    fi

    if [ ! -f "${INPUT_FILE}" ]; then
        echo "Unable to sign '${INPUT_FILE}'."
        exit 1
    fi

    warn "If needed, the passphrase for the signing key is: 'ultimaker'".
    echo "ultimaker" | eval "${GPG}" \
        "${GPGSIGPARAMS}" \
        --default-key "${SIGNING_KEY_ID}" \
        --detach-sign \
        --output "${INPUT_FILE}.sig" \
        "${INPUT_FILE}"

    cp "${INPUT_FILE}" "${OUTPUT_FILENAME}"
    cat "${INPUT_FILE}.sig" >> "${OUTPUT_FILENAME}"
    # Generate the signature size bytes. Stat gives a decimal string, which
    # printf turns into hex (without the 0x prefix). tac is then used to reverse
    # this string using some regex/separator magic to obtain a little-endian
    # unsigned decimal string. Finally xxd does a 'reverse' hexdump print to
    # get the actual bytes to append.
    printf "%08x" "$(stat -c "%s" "${INPUT_FILE}.sig")" | tac -rs .. | xxd -r -p >> "${OUTPUT_FILENAME}"
}

# This function checks if the parameter given is a known package in the list, and if it has a repository.
# If this is not the case, then it will abort the script with an error message
check_known_source_package() {
    local PACKAGE=`cat ${PACKAGE_LIST_FILE} | grep "^${1}[[:space:]]"`
    if [[ -z " ${PACKAGE}" ]]; then
        error "Package ${1} is not in the packages.list, so nothing is known about it.";
    fi
    local REPO=`echo ${PACKAGE} | awk '{ print $2 }'`
    if [[ -z "${REPO}" ]]; then
        error "Package ${1} does not have a source repository";
    fi
}

# Get the repository name for a package. Assumes the package exists.
get_repo() {
    cat ${PACKAGE_LIST_FILE} | grep "^${1}[[:space:]]" | awk '{ print $2 }'
}

# Ask the user if they are sure they want to continue. Called when we potentially do something destructive
# Aborts the script if the users says no.
ask_sure() {
    read -p "Are you sure? (y/n) " -n 1 REPLY
    echo ""
    if [[ "${REPLY}" != "y" ]]; then
        info "Abort."
        exit 255
    fi
}

# Make sure a git checkout is clean, make a fresh clone if there is none, else run all the git foo to clean it.
git_clean() {
    local GIT_PATH=${1}
    local GIT_REPO=${2}
    local GIT_BRANCH=${3}
    if [[ "${GIT_BRANCH}" == "" ]]; then
        GIT_BRANCH=master
    fi
    info "Making sure ${GIT_PATH} is clean and fresh from ${GIT_REPO} with branch ${GIT_BRANCH}"
    if [[ ! -e ${GIT_PATH} ]]; then
        git clone --recursive -b ${GIT_BRANCH} ${GIT_REPO} ${GIT_PATH}
    fi
    pushd ${GIT_PATH} > /dev/null
    git clean -f -x -d
    git reset --hard
    git checkout ${GIT_BRANCH}
    git pull --recurse-submodules
    popd > /dev/null
}

# Check if the tag given is found in the given local repository
git_check_tag() {
    if GIT_DIR=${2}/.git git rev-parse "${TAG_PREFIX}${1}" > /dev/null 2>&1; then
        error "Version ${1} already exists. Aborting."
    fi
}

git_add_tag() {
    GIT_DIR=${2}/.git git tag -a -m '' "${TAG_PREFIX}${1}"
    GIT_DIR=${2}/.git git push --tags
}

git_list_jira_issues_after_last_release() {
    pushd ${1} > /dev/null
    local TAG=`git describe --tags \`git rev-list --tags --abbrev=0 --max-count=1\``
    git log ${TAG}...HEAD --pretty=fuller | grep -oP '(EM|CURA|CL)-[0-9]+' | sort | uniq | sed -e 's#^#https://jira.ultimaker.com:8443/browse/#'
    popd > /dev/null
}

###########################
# Main workhorse functions
###########################
fetch_package() {
    PACKAGE=${1}
    REV=
    if [[ ! -z "${2+x}" ]]; then
        REV=${2};
    fi
    check_known_source_package ${PACKAGE}
    REPO=$( get_repo ${PACKAGE} )
    info "Going to do a fresh checkout of ${PACKAGE}/${REV} into ${PACKAGE_SOURCES}/${PACKAGE}"
    if [[ -e "${PACKAGE_SOURCES}/${PACKAGE}" ]]; then
        info "Directory ${PACKAGE_SOURCES}/${PACKAGE} exists, will erase before we continue."
        ask_sure
        rm -rf "${PACKAGE_SOURCES}/${PACKAGE}"
    fi
    if [ -n "${REV}" ]; then
        git clone --recursive "${REPO}" "${PACKAGE_SOURCES}/${PACKAGE}" --branch "${REV}"
    else
        git clone --recursive "${REPO}" "${PACKAGE_SOURCES}/${PACKAGE}"
    fi
    info "Sources are now located at: ${PACKAGE_SOURCES}/${PACKAGE}"
}

build_package() {
    local PACKAGE=${1}
    local VERSION=9999.99.99
    if [[ ! -z "${2+x}" ]]; then
        VERSION=${2}
    fi
    check_known_source_package ${PACKAGE}
    info "building package: ${PACKAGE}"
    # Check if the sources are available, if not, just fetch them first.
    if [[ ! -e "${PACKAGE_SOURCES}/${PACKAGE}" ]]; then
        fetch_package ${PACKAGE}
    fi
    # Check for our sysroot. We need it, usually.
    if [[ ! -e "${SYSROOT}" ]]; then
        info "Sysroot not found, building that first."
        build_sysroot
    fi

    # Now, we have two kind of build types. CMake based, as well as script based.
    # First check if we have a "build_for_ultimaker.sh" script, else check for a cmake file.
    if [[ -e "${PACKAGE_SOURCES}/${PACKAGE}/build_for_ultimaker.sh" ]]; then
        info "Building with build_for_ultimaker.sh script"
        pushd "${PACKAGE_SOURCES}/${PACKAGE}" > /dev/null
        rm -rf *.deb
        RELEASE_VERSION=${VERSION} SYSROOT=${SYSROOT} ./build_for_ultimaker.sh
        local DEB_RESULT_PATH=$(pwd)
        popd > /dev/null
    elif [[ -e "${PACKAGE_SOURCES}/${PACKAGE}/CMakeLists.txt" ]]; then
        info "Building with cmake"
        mkdir -p "${PACKAGE_SOURCES}/${PACKAGE}/_build_armhf"
        pushd "${PACKAGE_SOURCES}/${PACKAGE}/_build_armhf" > /dev/null

        # -DEMBEDDED yields warnings for packages that don't support it, only fdm_materials 
        cmake .. -DCMAKE_TOOLCHAIN_FILE=${TOOLCHAIN_FILE} -DCMAKE_INSTALL_PREFIX=/usr -DCPACK_PACKAGE_VERSION=${VERSION} -DEMBEDDED=ON
        # Before we build, we delete any lingering .deb file. So we can find our result
        rm -rf *.deb
        make package
        local DEB_RESULT_PATH=$(pwd)
        popd > /dev/null
    else
        error "No valid build method found for ${PACKAGE}"
    fi
    if [[ $(ls ${DEB_RESULT_PATH}/*.deb | wc -l) -ne 1 ]]; then
        error "None, or multiple .deb files found after building package. Aborting."
    fi
    DEB_FILE_RESULT=$(realpath $(ls ${DEB_RESULT_PATH}/*.deb))
}

build_sysroot() {
    info "Going to build sysroot for cross compiling"

    # Build the _multistrap.cfg file, with a generated list from packages.list, but remove everything that has a git reference, as we do not want those in our sysroot by default.
    local PACKAGES=`cat ${PACKAGE_LIST_FILE} | grep -v '^#' | grep -v git | cut -f1 | tr '\n' ' '`
    cat > ${TOOLS}/_sysroot_multistrap.cfg <<-EOF
[General]
arch=armhf
noauth=true
unpack=true
explicitsuite=false
aptsources=Debian
bootstrap=Debian DebianStable

[DebianStable]
packages=${PACKAGES}
packages=build-essential libdbus-1-dev libfreetype6-dev libreadline-dev libglib2.0-dev iptables-dev libxtables10 libgnutls28-dev libjpeg-dev libnfc-dev qtdeclarative5-dev
source=http://ftp.debian.com/debian
suite=jessie
components=main non-free
EOF

    #Build a sysroot with arm files so we have a target to build against.
    rm -rf ${SYSROOT}

    mkdir -p "${SYSROOT}/etc/apt/trusted.gpg.d"
    curl https://ftp-master.debian.org/keys/archive-key-8.asc | fakeroot apt-key --keyring "${SYSROOT}/etc/apt/trusted.gpg.d/jessie.gpg" add -

    /usr/sbin/multistrap -f ${TOOLS}/_sysroot_multistrap.cfg -d ${SYSROOT}

    # Fix up the symlinks in the sysroot, find all links that start with absolute paths
    #  and replace them with relative paths inside the sysroot.
    pushd ${SYSROOT} > /dev/null
    for F in `find -type l`; do
        local LINK=`readlink ${F} || echo ''`
        if [ ! -z "${LINK}" ]; then
            if [ "${LINK:0:1}" == "/" ]; then
                if [ -e "${SYSROOT}${LINK}" ]; then
                    rm "${F}"
                    ln --relative -sf "${SYSROOT}${LINK}" "${F}"
                fi
            fi
        fi
    done
    popd > /dev/null

    info "Finished building sysroot in: ${SYSROOT}"
    info "You can now use cmake -DCMAKE_TOOLCHAIN_FILE=${TOOLCHAIN_FILE} to build software"
}

# Reads the image_cleanup/${RELEASE_TYPE} file and handles the contents to remove files and complete directories
# Used by build_rootfs
process_release() {
    local TARGET="${1}"
    if [ ! -d "${TARGET}" ]; then
        error "Target ${TARGET} invalid!"
    fi
    local RELEASE_TYPE="${2}"
    if [ -z "${RELEASE_TYPE}" ]; then
        warn "RELEASE_TYPE is not set. Not modifying build."
        return
    fi
    local RELEASE_FILE="image_cleanup/${RELEASE_TYPE}"
    if [ ! -f "${RELEASE_FILE}" ]; then
        warn "RELEASE_TYPE file not found: '${RELEASE_FILE}'. Not modifying build."
        return
    fi

    info "Cleaning up image using ${RELEASE_FILE}..."
    while read -r LINE; do
        if [ -z "${LINE}" ]; then
            continue # Skip empty lines
        fi
        # Ignore comment lines (starting with #)
        if [[ "${LINE}" =~ ^# ]]; then
            continue
        fi
        if [ -e "${TARGET}/${LINE}" ]; then
            info "Removing file/directory ${TARGET}/${LINE}..."
            sudo chroot "${TARGET}" /bin/rm -rf "/${LINE}"
        else
            warn "Unable to remove non existing file/path ${TARGET}/${LINE}"
        fi
    done < "${RELEASE_FILE}";
}

# build_rootfs [version]
build_rootfs() {
    if [[ `whoami` != "root" ]]; then
        info "build image needs to run parts as root. Please enter your sudo password."
        sudo echo -n
    fi
    local VERSION=9999.99.99-DEV.`date +%Y%m%d`
    if [[ ! -z "${1+x}" ]]; then
        VERSION=${1}
    fi
    info "Building image"
    # First, make sure the package-repository is clean, fresh and new.
    # This takes a while for the first checkout, but after that, updates are quick.
    git_clean ${PACKAGE_REPO} ${PACKAGE_REPO_GIT} ${PACKAGE_REPO_BRANCH}

    local HTTP_PORT=$(( (RANDOM % 10000) + 1025 ))
    # We create an http server to server files to apt-get, so setup a directory for that.
    local HTTP_DIR=${BUILD_DIR}/http

    # The TARGET variable is used by the image configuration scripts.
    export TARGET=${BUILD_DIR}/target/

    # Clean up anything that might be lingering
    sudo rm -rf ${BUILD_DIR}

    # injecting our public key into the target's trusted keys for APT
    mkdir -p "${TARGET}/etc/apt/trusted.gpg.d"
    fakeroot apt-key --keyring "${TARGET}/etc/apt/trusted.gpg.d/ultimaker.gpg" add "${MYDIR}/${PUBLIC_KEY_FILE}"

    # Create the apt repository locations for the http server
    for DIST in official released override; do
        mkdir -p ${HTTP_DIR}/${DIST}/dists/stable/main/
        if [[ ${DIST} == "override" ]]; then
            mkdir -p ${OVERRIDE_DIR}
            ln -s ${OVERRIDE_DIR} ${HTTP_DIR}/${DIST}/dists/stable/main/binary-armhf
        else
            mkdir -p ${PACKAGE_REPO}/${DIST}
            ln -s ${PACKAGE_REPO}/${DIST} ${HTTP_DIR}/${DIST}/dists/stable/main/binary-armhf
        fi
        # Create the source directory because apt needs this, we do not provide source packages to apt.
        mkdir -p ${HTTP_DIR}/${DIST}/dists/stable/main/source
        touch ${HTTP_DIR}/${DIST}/dists/stable/main/source/Sources

        pushd ${HTTP_DIR}/${DIST} > /dev/null
        info "Generating package list for ${DIST}"
        dpkg-scanpackages dists/stable/main/binary-armhf > dists/stable/main/binary-armhf/Packages
        cat > Release.conf <<-EOF
APT::FTPArchive::Release::Origin "Ultimaker";
APT::FTPArchive::Release::Label "jedi-build";
APT::FTPArchive::Release::Suite "stable";
APT::FTPArchive::Release::Codename "debian";
APT::FTPArchive::Release::Architectures "armhf";
APT::FTPArchive::Release::Components "main";
APT::FTPArchive::Release::Description "jedi-build repository";
EOF
        apt-ftparchive -c=Release.conf release dists/stable > "dists/stable/Release"
        warn "${GPG_PASSPHRASE_WARN}"
        echo "ultimaker" | "${GPG}" \
            ${GPGSIGPARAMS} \
            --default-key ${SIGNING_KEY_ID} \
            --detach-sign \
            --output dists/stable/Release.gpg \
            "dists/stable/Release"
        popd > /dev/null
    done

    # Start our private http server.
    busybox httpd -p ${HTTP_PORT} -f -h ${HTTP_DIR} &
    HTTPD_PID=$!
    info "Started http server on port ${HTTP_PORT}"
    # Kill the server when this script exits
    at_exit 'kill $HTTPD_PID; echo "Stopped http server"' EXIT

    # Get the list of packages from our package file. As we do not want this per default in our updates.
    local PACKAGES=`cat ${PACKAGE_LIST_FILE} | grep -v '^#' | cut -f1 | tr '\n' ' '`
    # Generate the _multistap.cfg file, which defines which packages need to end up in the image, and from where.
    # We generate this file, as it needs to contain the http port, as well as a list of packages.
    # Instead of maintaining this file, we maintain a list of packages in packages.list so we can maintain that list with extra metadata.
    cat > ${BUILD_DIR}/_multistrap.cfg <<-EOF
[General]
arch=armhf
noauth=true
unpack=true
explicitsuite=false
bootstrap=OfficialDebian ReleasedPackages OverridePackages

[OfficialDebian]
source=http://127.0.0.1:${HTTP_PORT}/official
suite=stable
components=main
packages=${PACKAGES}

[ReleasedPackages]
source=http://127.0.0.1:${HTTP_PORT}/released
suite=stable
components=main
packages=${PACKAGES}
EOF

    if [[ -z "${1+x}" ]]; then
        # When building a local version (not a release) add the override repo
        cat >> ${BUILD_DIR}/_multistrap.cfg <<-EOF
[OverridePackages]
source=http://127.0.0.1:${HTTP_PORT}/override
suite=stable
components=main
packages=${PACKAGES}
EOF
    fi

    # Copy qemu if remote does not match current arch
    if [[ ${CURRENT_ARCH} != ${TARGET_ARCH} ]]; then
        if [[ ! $(which ${QEMU_BINARY}) ]]; then
            error "Error, no QEMU binary available, install qemu-user-static!"
        fi
        sudo mkdir -p ${TARGET}/usr/bin
        sudo cp ${QEMU_BINARY} ${TARGET}/usr/bin
    fi

    # Run multistrap to generate the rootfs
    info "Running multistrap to build rootfs"
    sudo /usr/sbin/multistrap -f ${BUILD_DIR}/_multistrap.cfg -d ${TARGET}

    # Run the scripts to configure the debian system.
    info "Finalizing rootfs"
    for F in image_customize_scripts/*.sh; do
        if [ -x ${F} ]; then
            info "Executing: ${F}"
            # run bash with -u and -e so it fails when any of it's commands fail.
            # else this script will continue when a setup step fails, causing broken builds.
            sudo -E bash -u -e ${F}
        fi
    done

    # Move the ouroborus installer module away from our image, as we don't want to include this in every image,
    # Only in specific installers.
    sudo mv ${TARGET}/ulti_installer/ouroboros.py ${BUILD_DIR}/ouroboros.py

    process_release "${TARGET}" "${RELEASE_TYPE}"

    # Insert the version number to the proper file
    echo $VERSION > ${BUILD_DIR}/ultimaker_version
    sudo cp ${BUILD_DIR}/ultimaker_version ${TARGET}/etc/ultimaker_version
    # Symlink for backwards compatibility. Version file used to be named jedi_version.
    # Remove this when all dependences on this file have been changed to point to the new file.
    sudo ln -sf ultimaker_version ${TARGET}/etc/jedi_version

    info "Compressing rootfs..."
    sudo tar -cJf ${BUILD_DIR}/${IMAGE_TAR_FILENAME}.tar.xz -C ${TARGET} .
    sudo chown `id -un`:`id -gn` ${BUILD_DIR}/${IMAGE_TAR_FILENAME}.tar.xz

    sudo mksquashfs "${TARGET}" "${BUILD_DIR}/${UPDATE_FILENAME_PREFIX}.squashfs" -comp xz
    sudo chown "$(id -un):$(id -gn)" "${BUILD_DIR}/${UPDATE_FILENAME_PREFIX}.squashfs"

    info "Created '${BUILD_DIR}/${IMAGE_TAR_FILENAME}.tar.xz' and '${BUILD_DIR}/${UPDATE_FILENAME_PREFIX}.squashfs'"
}

# Create all the required SD card images
create_sd_images() {
    mkdir -p ${IMAGES_DIR}
    create_sd_image ${IMAGES_DIR}/run_from_sd.img "sd"
    create_sd_image ${IMAGES_DIR}/recovery.img "emmc"

    # If Taranis is part of the package list, build a Taranis image
    if [ -z "${PACKAGE_LIST_FILE##*taranis*}" ]; then
        info "Building images for Taranis NFC programming stations"
        create_sd_image ${IMAGES_DIR}/Taranis_installer.img "emmc" 1866 "yes"
    else
        info "Building images for UM3 based printers"
        create_sd_image ${IMAGES_DIR}/UM3_installer.img "emmc" 9066 "yes"
        create_sd_image ${IMAGES_DIR}/UM3extended_installer.img "emmc" 9511 "yes"
        create_sd_image ${IMAGES_DIR}/S5_installer.img "emmc" 9051 "yes"
    fi
}

# Create the SD card image.
# create_sd_image [target_filename] [type:sd/emmc] [target_bom_number_for_installer] [yes/no for allowing mac address installation]
create_sd_image() {
    local STORAGE=${2}
    local IMAGE_FILENAME=${1}
    local BUILD_DIR=${MYDIR}/_image_build
    local ROOT_DIR=${BUILD_DIR}/img_root
    local INSTALLER_DIR=${ROOT_DIR}/ulti_installer # This file needs to be in sync with jedi-installer

    local FILE_SYSTEM_SIZE_BYTES=$(lzma --robot -l --format=auto ${BUILD_DIR}/${IMAGE_TAR_FILENAME}.tar.xz | grep totals | awk '{ print $5 }')
    local COMPRESSED_FILE_SYSTEM_SIZE_KB=$(du -k ${BUILD_DIR}/${IMAGE_TAR_FILENAME}.tar.xz | cut -f1)
    local IMAGE_SIZE_BYTES=$(expr ${FILE_SYSTEM_SIZE_BYTES} \* 120 / 100 + ${COMPRESSED_FILE_SYSTEM_SIZE_KB} \* 1024 + ${MBR_ROOTFS_START} \* 512)
    local IMAGE_SIZE_MB=$(expr ${IMAGE_SIZE_BYTES} / 1000000)

    info "Creating basic image"
    # Create an empty image with 2 partitions
    dd if=/dev/zero of=${IMAGE_FILENAME} bs=1M count=${IMAGE_SIZE_MB}
    # --force is needed on older version of sfdisk else sfdisk refuses due to
    # the partitions not properly being aligned with cylinder count (which does not matter).
    /sbin/sfdisk -uS --force ${IMAGE_FILENAME} <<-EOT
${MBR_BOOTLOADER_START},${MBR_BOOTLOADER_SIZE},L
${MBR_ROOTFS_START},,L
EOT
    info "Setting up loopback mounts"
    BOOT_LOOP_DEVICE=$(sudo losetup --show -o `expr ${MBR_BOOTLOADER_START} \* 512` --sizelimit `expr ${MBR_BOOTLOADER_SIZE} \* 512` -f ${IMAGE_FILENAME})
    echo "BOOT: ${BOOT_LOOP_DEVICE}"
    ROOT_LOOP_DEVICE=$(sudo losetup --show -o `expr ${MBR_ROOTFS_START} \* 512` -f ${IMAGE_FILENAME})
    echo "ROOT: ${ROOT_LOOP_DEVICE}"
    at_exit "sudo losetup -d ${BOOT_LOOP_DEVICE}"
    at_exit "sudo losetup -d ${ROOT_LOOP_DEVICE}"

    info "Creating filesystems"
    sudo mkfs.ext4 ${BOOT_LOOP_DEVICE} -L installer_boot -O ^extents,^64bit
    sudo mkfs.ext4 ${ROOT_LOOP_DEVICE} -L installer_root

    info "Mounting empty filesystems"
    sudo mkdir -p ${ROOT_DIR}
    sudo mount ${ROOT_LOOP_DEVICE} ${ROOT_DIR}
    sudo mkdir -p ${ROOT_DIR}/boot
    sudo mount ${BOOT_LOOP_DEVICE} ${ROOT_DIR}/boot
    at_exit "sudo umount ${ROOT_LOOP_DEVICE} && rmdir ${ROOT_DIR}"
    at_exit "sudo umount ${BOOT_LOOP_DEVICE}"

    info "Extracting rootfs to root partition"
    pushd ${ROOT_DIR} > /dev/null
    sudo tar --warning=none -xJf ${BUILD_DIR}/${IMAGE_TAR_FILENAME}.tar.xz
    popd > /dev/null
    # TODO Replace with debian's 'flash-kernel' scripts
    info "Preparing target boot environment"
    if [[ ${STORAGE} = "sd" ]]; then
        # Install the bootloader startup scrip
        sudo cp ${ROOT_DIR}/boot/boot_mmc.scr ${ROOT_DIR}/boot/boot.scr
    elif [[ ${STORAGE} = "emmc" ]] || [[ ${STORAGE} = "nand" ]]; then
        # Install the bootloader startup script
        sudo cp ${ROOT_DIR}/boot/boot_installer.scr ${ROOT_DIR}/boot/boot.scr
        # Put the compressed rootfs.tar in the image.
        sudo cp ${BUILD_DIR}/${IMAGE_TAR_FILENAME}.tar.xz ${INSTALLER_DIR}
        # Set installer as default target in this image
        sudo chroot ${ROOT_DIR} systemctl enable jedi-installer.service
        sudo chroot ${ROOT_DIR} systemctl set-default install.target
        # Mask the getty service so we do not get a terminal on the display.
        sudo chroot ${ROOT_DIR} systemctl mask getty
        if [[ ! -z ${3+x} ]]; then
            # Write the BOM nummer for this installer into the installer
            sudo sh -c "echo '${3}' > ${ROOT_DIR}/ulti_installer/bom.txt"
            if [[ ${4} = "yes" ]]; then
                # Install the ouroboros python module, so we can install mac addresses with this image.
                sudo cp ${BUILD_DIR}/ouroboros.py ${ROOT_DIR}/ulti_installer/ouroboros.py
            fi
        fi
        # Make the root filesystem readonly for the SD card installer
        sudo sh -c "sed -i 's/\(rootfs *\)defaults/\1ro/' ${ROOT_DIR}/etc/fstab"
        # Make the mtab file a symlink, with a readonly filesystem, mount cannot update this file
        # Causing umount problems.
        sudo sh -c "ln -sf /proc/mounts ${ROOT_DIR}/etc/mtab"
    fi

    info "Installing bootloader into image"
    # Install the u-boot image on the proper sectors in the SD image (required by the BROM of the A20 CPU)
    # Never write beyond 1MiB, as this is where our partitions start (MBR_BOOTLOADER_START)
    dd if=${ROOT_DIR}/boot/u-boot-sunxi-with-spl.bin of=${IMAGE_FILENAME} bs=1024 seek=8 count=1024 conv=notrunc
    if [ -f ${ROOT_DIR}/boot/${UBOOTENV} ]; then
        dd if=${ROOT_DIR}/boot/${UBOOTENV} of=${IMAGE_FILENAME} bs=1024 seek=544 count=128 conv=notrunc,fsync
    fi

    sync

    info "Created ${IMAGE_FILENAME} as raw bootable ${STORAGE} image"
}

#################################
# Main entry point of the script
#################################

# Check if we gave any parameters
if [[ $# -lt 1 ]]; then
    display_usage
fi

if [[ ! -f "${PACKAGE_LIST_FILE}" ]]; then
    error "Package list file ${PACKAGE_LIST_FILE} not found!"
fi

# Use the first parameter to see what we need to do
case ${1} in
fetch)
    if [[ $# -lt 2 ]]; then
        display_usage;
    fi
    PACKAGE=""
    if [ ! -z "${2+x}" ]; then
        PACKAGE="${2}"
    fi
    REV=""
    if [ ! -z "${3+x}" ]; then
        REV="${3}"
    fi
    if [ "${PACKAGE}" = "all" ]; then
        for __PACKAGE in $(cat "${PACKAGE_LIST_FILE}" | grep -v '^#' | cut -f1); do
            if [[ ! -z "$(get_repo ${__PACKAGE})" ]]; then
                fetch_package ${__PACKAGE} ${REV}
            fi
        done
    else
        fetch_package "${PACKAGE}" "${REV}"
    fi
    ;;
build)
    if [[ $# -lt 2 ]]; then
        display_usage;
    fi
    if [[ ${2} = "image" ]]; then
        # Clear the sudo password authentication cache after we finished building.
        at_exit "sudo -K"

        build_gpgsig_params
        warn "${GPG_PASSPHRASE_WARN}"
        # adding ultimakers private key to the keyring
        if ! "${GPG}" --batch --yes --import --passphrase "ultimaker" "${MYDIR}/${PRIVATE_KEY_FILE}" ; then
            echo "Key failed to import, on GPGv2.0.x this could mean it probably already exists."
        fi
        DATE=`date +%Y%m%d`
        build_rootfs
        create_sd_images

        cp ${MYDIR}/_image_build/${IMAGE_TAR_FILENAME}.tar.xz ${IMAGES_DIR}/${IMAGE_TAR_FILENAME}-TEST.${DATE}.tar.xz

        warn "${GPG_PASSPHRASE_WARN}"
        echo "ultimaker" | "${GPG}" \
            ${GPGSIGPARAMS} \
            --default-key ${SIGNING_KEY_ID} \
            --detach-sign \
            --output ${IMAGES_DIR}/rootfs-TEST.${DATE}.tar.xz.sig \
            ${IMAGES_DIR}/rootfs-TEST.${DATE}.tar.xz

        append_signature "${BUILD_DIR}/${UPDATE_FILENAME_PREFIX}.squashfs" "${BUILD_DIR}/${UPDATE_FILENAME_PREFIX}.squashfs.swu"
        cp "${BUILD_DIR}/${UPDATE_FILENAME_PREFIX}.squashfs.swu" "${IMAGES_DIR}/${UPDATE_FILENAME_PREFIX}-TEST.${DATE}.swu"

        info "Finished building TEST image '${IMAGES_DIR}/${IMAGE_TAR_FILENAME}-TEST.${DATE}.tar.xz' and '${IMAGES_DIR}/${UPDATE_FILENAME_PREFIX}-TEST.${DATE}.swu'."
    elif [[ ${2} == "all" ]]; then
        for PACKAGE in $(cat ${PACKAGE_LIST_FILE} | grep -v '^#' | cut -f1); do
            if [[ -e ${PACKAGE_SOURCES}/${PACKAGE} ]]; then
                build_package ${PACKAGE}
                mkdir -p ${OVERRIDE_DIR}
                cp ${DEB_FILE_RESULT} ${OVERRIDE_DIR}
                info "Final package placed in: ${OVERRIDE_DIR}/$(basename ${DEB_FILE_RESULT})"
            fi
        done
    else
        build_package ${2}
        mkdir -p ${OVERRIDE_DIR}
        cp ${DEB_FILE_RESULT} ${OVERRIDE_DIR}
        info "Final package placed in: ${OVERRIDE_DIR}/$(basename ${DEB_FILE_RESULT})"
    fi

    ;;
release)
    if [[ $# -lt 3 ]]; then
        display_usage;
    fi
    PACKAGE=${2}
    VERSION=${3}
    REV=
    if [[ "${PACKAGE}" = "image" ]]; then
        DATE=`date +%Y%m%d`
        build_rootfs ${VERSION}.${DATE}
        create_sd_images
        # filenames must match the filename generated by create_sd_images!
        mv ${IMAGES_DIR}/run_from_sd.img           ${IMAGES_DIR}/run_from_sd-${VERSION}.${DATE}.img
        mv ${IMAGES_DIR}/UM3_installer.img         ${IMAGES_DIR}/UM3_installer-${VERSION}.${DATE}.img
        mv ${IMAGES_DIR}/UM3extended_installer.img ${IMAGES_DIR}/UM3extended_installer-${VERSION}.${DATE}.img
        mv ${IMAGES_DIR}/S5_installer.img          ${IMAGES_DIR}/S5_installer-${VERSION}.${DATE}.img
        mv ${IMAGES_DIR}/recovery.img              ${IMAGES_DIR}/recovery-${VERSION}.${DATE}.img

        cp ${MYDIR}/_image_build/${IMAGE_TAR_FILENAME}.tar.xz ${IMAGES_DIR}/${IMAGE_TAR_FILENAME}-${VERSION}.${DATE}.tar.xz

        warn "${GPG_PASSPHRASE_WARN}"
        echo "ultimaker" | "${GPG}" \
            ${GPGSIGPARAMS} \
            --default-key ${SIGNING_KEY_ID} \
            --detach-sign \
            --output ${IMAGES_DIR}/rootfs-${VERSION}.${DATE}.tar.xz.sig \
            ${IMAGES_DIR}/rootfs-${VERSION}.${DATE}.tar.xz
        info "Finished building release image"

        append_signature "${BUILD_DIR}/${UPDATE_FILENAME_PREFIX}.squashfs" "${BUILD_DIR}/${UPDATE_FILENAME_PREFIX}.squashfs.swu"
        cp "${BUILD_DIR}/${UPDATE_FILENAME_PREFIX}.squashfs.swu" "${IMAGES_DIR}/${UPDATE_FILENAME_PREFIX}-${VERSION}.${DATE}.swu"

        info "Finished building release image '${IMAGES_DIR}/${IMAGE_TAR_FILENAME}-${VERSION}.${DATE}.tar.xz' and '${IMAGES_DIR}/${UPDATE_FILENAME_PREFIX}-${VERSION}.${DATE}.swu'."
    else
        if [[ ! -z "${4+x}" ]]; then
            REV=$4;
        fi
        check_known_source_package ${PACKAGE}
        if [[ ! "${VERSION}" =~ ^[0-9]+\.[0-9]+\.[0-9]+(\-[0-9]+)?$ ]]; then
            warn "Version number ${VERSION} does not match x.x.x or x.x.x-x scheme."
            ask_sure
        fi

        fetch_package ${PACKAGE} ${REV}
        git_check_tag "${VERSION}" "${PACKAGE_SOURCES}/${PACKAGE}"
        build_package ${PACKAGE} ${VERSION}

        info "Listing issues that have been fixed after last release:"
        JIRA_ISSUE_LOG=$(git_list_jira_issues_after_last_release "${PACKAGE_SOURCES}/${PACKAGE}")
	echo ${JIRA_ISSUE_LOG}
        info "-------------------------------------------------------"

        # Add a tag to the local repository. This also pushes the tag. We do this before we commit the .deb to the releases repo. As we rather have a tag without a .deb then a .deb without a tag.
        git_add_tag "${VERSION}" "${PACKAGE_SOURCES}/${PACKAGE}"

        # First, make sure the package-repository is clean, fresh and new.
        # This takes a while for the first checkout, but after that, updates are quick.
        git_clean ${PACKAGE_REPO} ${PACKAGE_REPO_GIT} ${PACKAGE_REPO_BRANCH}
        info "Adding new release to package repository"
        pushd ${PACKAGE_REPO} > /dev/null
        cp ${DEB_FILE_RESULT} ${PACKAGE_REPO}/released/
        git add ${PACKAGE_REPO}/released/`basename ${DEB_FILE_RESULT}`
        git commit -m "Released ${PACKAGE} version ${VERSION}" -m "${JIRA_ISSUE_LOG}"
        git push
        popd > /dev/null
        info "Release done!"
    fi
    ;;
deploy)
    if [[ $# -lt 3 ]]; then
       display_usage;
    fi
    PACKAGE=${2}
    TARGET_HOST=${3}
    # Parameters for ssh, to disable known host checking. Allow for interaction with ssh without user interaction
    SSH_PARAMS="-o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null"
    SSH="sshpass -p ultimaker ssh ${SSH_PARAMS} root@${TARGET_HOST} /bin/sh -l -c"

	# Test if host is reachable
	if ! ping -c 1 -W 3 ${TARGET_HOST} &> /dev/null; then 
		error "Error: Target ${TARGET_HOST} is unreachable."
	fi

    if [ ${PACKAGE} = "image" ]; then
        # Special case when the 'package' is not a package at all, but the full image is needed to be deployed.
        if [ ! -f "${BUILD_DIR}/${IMAGE_TAR_FILENAME}.tar.xz" ]; then
            info "Missing rootfs file, building new rootfs"
            build_rootfs
        fi

        TARGET_IMAGE_VERSION=`${SSH} "\"cat /etc/jedi_version\""`
        info "Target image version: ${TARGET_IMAGE_VERSION}"
        if [[ ${TARGET_IMAGE_VERSION} != "MOD-"* ]]; then
            TARGET_HOSTNAME=`${SSH} "\"hostname\""`
            warn "Target machine does not contain modifications yet."
            warn "This could mean your IP address is changed, or you did a fresh update on your machine."
            warn "Be sure that your IP address is correct before you continue!"
            warn "This is machine: ${TARGET_HOSTNAME}"
            ask_sure
        fi

        info "Copying ${IMAGE_TAR_FILENAME} to ${TARGET_HOST}"
        sshpass -p ultimaker scp ${SSH_PARAMS} ${BUILD_DIR}/${IMAGE_TAR_FILENAME}.tar.xz root@${TARGET_HOST}:/tmp/rootfs.tar.xz
        info "Starting system update ..."
        ${SSH} "\"systemctl isolate update.rootfs.target\""
        info "System update started, the target will reboot in a few minutes"
    else
        build_package ${PACKAGE}

        TARGET_IMAGE_VERSION=`${SSH} "\"cat /etc/jedi_version\""`
        info "Target image version: ${TARGET_IMAGE_VERSION}"
        if [[ ${TARGET_IMAGE_VERSION} != "MOD-"* ]]; then
            TARGET_HOSTNAME=`${SSH} "\"hostname\""`
            warn "Target machine does not contain modifications yet."
            warn "This could mean your IP address is changed, or you did a fresh update on your machine."
            warn "Be sure that your IP address is correct before you continue!"
            warn "This is machine: ${TARGET_HOSTNAME}"
            ask_sure
            ${SSH} "\"echo 'MOD-${TARGET_IMAGE_VERSION}' > /etc/jedi_version\""
        fi

        info "Copying ${DEB_FILE_RESULT} to ${TARGET_HOST}"
        sshpass -p ultimaker scp ${SSH_PARAMS} ${DEB_FILE_RESULT} root@${TARGET_HOST}:/tmp
        info "Stopping griffin services"
        ${SSH} "\"systemctl stop griffin.* nginx\"" || true
        info "Installing new package"
        ${SSH} "\"dpkg -i /tmp/$(basename ${DEB_FILE_RESULT})\""
        info "Installed new package"
        ${SSH} "\"systemctl start griffin.* nginx\"" || true
        TARGET_NAME=`${SSH} "\"hostname\""`
        info "Deployed ${PACKAGE} from location ${PACKAGE_SOURCES}/${PACKAGE} on ${TARGET_NAME}"
        pushd ${PACKAGE_SOURCES}/${PACKAGE} > /dev/null
        info "Log:"
        git log -n 1
        info "Commit: $(git describe --dirty --always)"
        popd > /dev/null
    fi
    ;;
listissues)
    if [[ $# -lt 2 ]]; then
       display_usage;
    fi
    PACKAGE=${2}
    info "Issue numbers in source that are not yet released:"
    git_list_jira_issues_after_last_release "${PACKAGE_SOURCES}/${PACKAGE}"
    ;;
list-updated-packages)
    info "Making sure all latest remote commits are known."
    for PACKAGE in $(cat ${PACKAGE_LIST_FILE} | grep -v '^#' | cut -f1); do
        if [[ ! -z "$(get_repo $PACKAGE)" ]]; then
            if [[ -e "${PACKAGE_SOURCES}/${PACKAGE}" ]]; then
                pushd "${PACKAGE_SOURCES}/${PACKAGE}" > /dev/null
                git fetch
                popd > /dev/null
            else
                fetch_package ${PACKAGE}
            fi
        fi
    done
    info "Checking all packages for commits after the latest release"
    for PACKAGE in $(cat ${PACKAGE_LIST_FILE} | grep -v '^#' | cut -f1); do
        if [[ ! -z "$(get_repo $PACKAGE)" ]]; then
            pushd "${PACKAGE_SOURCES}/${PACKAGE}" > /dev/null
            LATEST_TAG=$(git tag | grep ${TAG_PREFIX} | tail -n 1)
            if [[ -z "${LATEST_TAG}" ]]; then
                LATEST_TAG=$(git tag | tail -n 1)
            fi
            DIFF_STAT=$(git diff --shortstat ${LATEST_TAG} origin/HEAD)
            if [[ ! -z "${DIFF_STAT}" ]]; then
                echo ${PACKAGE} ${LATEST_TAG} ${DIFF_STAT}
            fi
            popd > /dev/null
        fi
    done
    ;;
sysroot)
    build_sysroot
    ;;
*)
    info "Unknown command: ${1}"
    info ""
    display_usage
esac
